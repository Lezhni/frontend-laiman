$(document).ready(function() {
});

$('form').submit(function(e) {

	var form = $(this);

	var name = form.children('.name').val();
	var phone = form.children('.phone').val();
	var message = form.children('.message').val();

	$.post('send.php', {
		name: name,
		phone: phone,
		message: message
		},
		function(data) {
			if(data === 'sended') {
				form.children('.name').val('');
				form.children('.phone').val('');
				form.children('.message').val('');
				alert('Ваше сообщение отправлено. В ближайшее время мы с Вами свяжемся');
			} else {
				alert('Сообщение не отправлено');
			}
		});

	return false;

});